#######
#TCS PARAMETERS FILE
#######

power_meter_output_low = {'TCS_ITMX':0.19,'TCS_ITMY':0.19}
power_meter_output_high = {'TCS_ITMX':0.22,'TCS_ITMY':0.22}


######
# KF PARAMETERS
######

# Ring Heater params
D1_rh = 0.3612
D2_rh = 1.9300
N1_rh = 0.3612
N2_rh = 2.5660

# Self Heating params
D1_self = 0.3521
D2_self = 1.912
N1_self = 0.3523
N2_self = 1.4245

# CO2 Laser params
D1_co2 = 1.1961
D2_co2 = 2.1905
N1_co2 = 1.1865
N2_co2 = 1.4303
