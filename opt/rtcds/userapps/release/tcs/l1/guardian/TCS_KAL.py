# Kalman filtering to correct thermo-optical aberrations

# Kalman filtering:
# - Predictor: Use info from the previous state to predict the next.
#              Use double exponential models of spherical power as
#              our aberration state space model.
# - Corrector: Use HWS aberration measurments to update predictions

# Variables:
# - X: state
# - P: state covariance
# - Z: measurements
# - U: actuator controls

# - A: state transition matrix
# - B: input-control matrix
# - C: state-to-observable matrix

# - Q: process noise covariance
# - R: measurement noise covariance
# - K: Kalman gain
# - w: process noise standard deviation
# - v: measurement noise standard deviation

# ==========================================================
#           Import Modules and Define Functions
# ==========================================================

from numpy import dot, linalg, array, zeros, eye, ix_
from guardian import GuardState, GuardStateDecorator
import cdsutils as cdu
import time
import tcsconfig
from ezca import Ezca

#JCB, remove this hack once better solution found
#import epics

#from ezca_automon import EzcaAccessor

## Kalman Filtering Functions
# X_kminus, P_kminus:  state and cov matrices from the previous state,
# X_prior, P_prior:    a priori estimate of the state and cov matrices
# X_post, P_post:      a posteriori estimate of the state and cov matrices,

def kf_predictor(X_kminus, P_kminus, U, A, B, Q):
    X_prior = dot(A, X_kminus) + dot(B, U)
    P_prior = dot(A, dot(P_kminus, A.T)) + Q
    return (X_prior, P_prior)

def kf_corrector(X_prior, P_prior, Z, C, R):
    S = dot(C, dot(P_prior, C.T)) + R
    K = dot(P_prior, dot(C.T, linalg.inv(S)))   # Kalman gain
    Y = Z - dot(C, X_prior)
    X_post = X_prior + dot(K, Y)
    P_post = P_prior - dot(K, dot(C, P_prior))
    return (X_post, P_post)


## Model Setup Functions
# Double exponential models of spherical power
def exp_model_matrices(dt, D1, D2, N1, N2):
    A = array([[1., dt], [-D1 * dt, 1. - D2 * dt]])
    B = array([[0., 0.], [N1 * dt, N2 * dt]])
    return (A, B)


# ==========================================================
#                          States
# ==========================================================

# initial request on initialization
request = 'KAL_EST_RUNNING'
# nominal operating state
nominal = 'KAL_EST_RUNNING'

class STOP(GuardState):
    """
    Stop Kalman filtering

    """
    index = 2
    goto = True
    request = True
    def main(self):
        return True

    def run(self):
        return True


class KAL_EST_RUNNING(GuardState):
    """
    Start Kalman filtering with default models

    """
    index = 1
    goto = True
    request = True
    def main(self):
        ### Initialize KF by defining model matrices
        self.dt = 1     # KF output rate

        ## Define state space model
        # ring heater
        A_rh, B_rh = exp_model_matrices(self.dt,
        		tcsconfig.D1_rh, tcsconfig.D2_rh, tcsconfig.N1_rh, tcsconfig.N2_rh)

        # self heating
        A_self, B_self = exp_model_matrices(self.dt,
        		tcsconfig.D1_self, tcsconfig.D2_self, tcsconfig.N1_self, tcsconfig.N2_self)

        # CO2 laser
        A_co2, B_co2 = exp_model_matrices(self.dt,
        		tcsconfig.D1_co2, tcsconfig.D2_co2, tcsconfig.N1_co2, tcsconfig.N2_co2)

        ## Construct model matrices for ITMs
        self.A_ITM = zeros((6, 6))
        self.A_ITM[ix_([0,1], [0,1])] = A_rh
        self.A_ITM[ix_([2,3], [2,3])] = A_self
        self.A_ITM[ix_([4,5], [4,5])] = A_co2

        self.B_ITM = zeros((6, 6))
        self.B_ITM[ix_([0,1], [0,1])] = B_rh
        self.B_ITM[ix_([2,3], [2,3])] = B_self
        self.B_ITM[ix_([4,5], [4,5])] = B_co2

        self.C_ITM = array([[1, 0, 1, 0, 1, 0]])

        ## Construct model matrices for ETMs
        self.A_ETM = zeros((4, 4))
        self.A_ETM[ix_([0,1], [0,1])] = A_rh
        self.A_ETM[ix_([2,3], [2,3])] = A_self

        self.B_ETM = zeros((4, 4))
        self.B_ETM[ix_([0,1], [0,1])] = B_rh
        self.B_ETM[ix_([2,3], [2,3])] = B_self

        self.C_ETM = array([[1, 0, 1, 0]])

        ## Initialize state and cov matrices (initialization doesn't matter much)
        self.X_post_ITMX = array([[0], [1e-3], [0], [1e-3], [0], [1e-3]])
        self.P_post_ITMX = eye(6)

        self.X_post_ITMY = array([[0], [1e-3], [0], [1e-3], [0], [1e-3]])
        self.P_post_ITMY = eye(6)

        self.X_post_ETMX = array([[0], [1e-3], [0], [1e-3]])
        self.P_post_ETMX = eye(4)

        self.X_post_ETMY = array([[0], [1e-3], [0], [1e-3]])
        self.P_post_ETMY = eye(4)

        ## Set measurement and process noise TODO,for each test mass, currently tuned for ITMY
        wix = 0.655e-6                      # process std dev
        vix = 19.38e-6                      # meas std dev
        wiy = 0.655e-6                      # process std dev
        viy = 19.38e-6                      # meas std dev
        wex = 0.655e-6                      # process std dev
        vex = 19.38e-6                      # meas std dev
        wey = 0.655e-6                      # process std dev
        vey = 19.38e-6                      # meas std dev
        ezca['TCS-SIM_ITMX_SUB_DEFOCUS_KALMAN_APOST_PROC_STDDEV'] = wix
        ezca['TCS-SIM_ITMX_SUB_DEFOCUS_KALMAN_APOST_MEAS_STDDEV'] = vix
        ezca['TCS-SIM_ITMY_SUB_DEFOCUS_KALMAN_APOST_PROC_STDDEV'] = wiy
        ezca['TCS-SIM_ITMY_SUB_DEFOCUS_KALMAN_APOST_MEAS_STDDEV'] = viy
        ezca['TCS-SIM_ETMX_SUB_DEFOCUS_KALMAN_APOST_PROC_STDDEV'] = wex
        ezca['TCS-SIM_ETMX_SUB_DEFOCUS_KALMAN_APOST_MEAS_STDDEV'] = vex
        ezca['TCS-SIM_ETMY_SUB_DEFOCUS_KALMAN_APOST_PROC_STDDEV'] = wey
        ezca['TCS-SIM_ETMY_SUB_DEFOCUS_KALMAN_APOST_MEAS_STDDEV'] = vey

        self.Q_ITMX = wix**2 * dot(self.B_ITM, self.B_ITM.T)  # process cov matrix
        self.Q_ITM = wiy**2 * dot(self.B_ITM, self.B_ITM.T)  # process cov matrix
        self.Q_ETMX = wex**2 * dot(self.B_ETM, self.B_ETM.T)
        self.Q_ETM = wey**2 * dot(self.B_ETM, self.B_ETM.T)
        self.R_IX = vix**2                                      # meas cov matrix
        self.R = viy**2                                      # meas cov matrix
        self.R_EX = vex**2                                      # meas cov matrix
        self.R_EY = vey**2                                      # meas cov matrix

        ## Suppress logs of writing to channels
        Ezca(logger=False).export()

        ## Set up input controls
#        self.power_rh_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_RH_LF_OUTPUT']  # This Kalman filter uses the simulation model to 
        self.power_rh_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_RH_SS_OUTPUT']   # predict the thermal transient, if inputs are used the 
        self.power_self_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_SELF_OUTPUT']  # thermal transient is not correctly modelled.   In th future the 
        self.power_co2_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_CO2_OUTPUT']    # Kalman filter should correctly model the thermal transient
        #self.power_rh_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_RH_LF_INMON']  # Kalman filter should work on these power inputs
        #self.power_self_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_SELF_INMON']
        #self.power_co2_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_CO2_INMON']

        self.power_rh_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_RH_LF_OUTPUT']
        self.power_self_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_SELF_OUTPUT']
        self.power_co2_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_CO2_OUTPUT']
        #self.power_rh_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_RH_LF_INMON']
        #self.power_self_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_SELF_INMON']
        #self.power_co2_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_CO2_INMON']

        self.power_rh_ETMX = ezca['TCS-SIM_ETMX_SUB_DEFOCUS_RH_LF_OUTPUT']
        self.power_self_ETMX = ezca['TCS-SIM_ETMX_SUB_DEFOCUS_SELF_OUTPUT']
        self.power_co2_ETMX = ezca['TCS-SIM_ETMX_SUB_DEFOCUS_CO2_OUTPUT']
        #self.power_rh_ETMX = ezca['TCS-SIM_ETMX_SUB_DEFOCUS_RH_LF_INMON']
        #self.power_self_ETMX = ezca['TCS-SIM_ETMX_SUB_DEFOCUS_SELF_INMON']
        #self.power_co2_ETMX = ezca['TCS-SIM_ETMX_SUB_DEFOCUS_CO2_INMON']

        self.power_rh_ETMY = ezca['TCS-SIM_ETMY_SUB_DEFOCUS_RH_LF_OUTPUT']
        self.power_self_ETMY = ezca['TCS-SIM_ETMY_SUB_DEFOCUS_SELF_OUTPUT']
        self.power_co2_ETMY = ezca['TCS-SIM_ETMY_SUB_DEFOCUS_CO2_OUTPUT']
        #self.power_rh_ETMY = ezca['TCS-SIM_ETMY_SUB_DEFOCUS_RH_LF_INMON']
        #self.power_self_ETMY = ezca['TCS-SIM_ETMY_SUB_DEFOCUS_SELF_INMON']
        #self.power_co2_ETMY = ezca['TCS-SIM_ETMY_SUB_DEFOCUS_CO2_INMON']

        self.start = time.time()

        return True

    def run(self):
        ## Read in HWS measurements
        Z_ITMX = ezca['TCS-ITMX_HWS_PROBE_SPHERICAL_POWER']
        Z_ITMY = ezca['TCS-ITMY_HWS_PROBE_SPHERICAL_POWER']
        Z_ETMX = ezca['TCS-ETMX_HWS_PROBE_SPHERICAL_POWER']
        Z_ETMY = ezca['TCS-ETMY_HWS_PROBE_SPHERICAL_POWER']

        ## Read in input control powers
        # ITMX
        power_rh_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_RH_LF_OUTPUT']
        power_self_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_SELF_OUTPUT']
        power_co2_ITMX = ezca['TCS-SIM_ITMX_SUB_DEFOCUS_CO2_OUTPUT']

        dpdt_rh_ITMX = (power_rh_ITMX - self.power_rh_ITMX) / self.dt
        dpdt_self_ITMX = (power_self_ITMX - self.power_self_ITMX) / self.dt
        dpdt_co2_ITMX = (power_co2_ITMX - self.power_co2_ITMX) / self.dt

        U_ITMX = array([[power_rh_ITMX], [dpdt_rh_ITMX], [power_self_ITMX],
        	[dpdt_self_ITMX], [power_co2_ITMX], [dpdt_co2_ITMX]])

        self.power_rh_ITMX = power_rh_ITMX
        self.power_self_ITMX = power_self_ITMX
        self.power_co2_ITMX = power_co2_ITMX

        # ITMY
        power_rh_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_RH_LF_OUTPUT']
        power_self_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_SELF_OUTPUT']
        power_co2_ITMY = ezca['TCS-SIM_ITMY_SUB_DEFOCUS_CO2_OUTPUT']

        dpdt_rh_ITMY = (power_rh_ITMY - self.power_rh_ITMY) / self.dt
        dpdt_self_ITMY = (power_self_ITMY - self.power_self_ITMY) / self.dt
        dpdt_co2_ITMY = (power_co2_ITMY - self.power_co2_ITMY) / self.dt

        U_ITMY = array([[power_rh_ITMY], [dpdt_rh_ITMY], [power_self_ITMY],
        	[dpdt_self_ITMY], [power_co2_ITMY], [dpdt_co2_ITMY]])

        self.power_rh_ITMY = power_rh_ITMY
        self.power_self_ITMY = power_self_ITMY
        self.power_co2_ITMY = power_co2_ITMY

        # ETMX
        power_rh_ETMX = ezca['TCS-SIM_ETMX_SUB_DEFOCUS_RH_LF_OUTPUT']
        power_self_ETMX = ezca['TCS-SIM_ETMX_SUB_DEFOCUS_SELF_OUTPUT']

        dpdt_rh_ETMX = (power_rh_ETMX - self.power_rh_ETMX) / self.dt
        dpdt_self_ETMX = (power_self_ETMX - self.power_self_ETMX) / self.dt

        U_ETMX = array([[power_rh_ETMX], [dpdt_rh_ETMX], [power_self_ETMX], [dpdt_self_ETMX]])

        self.power_rh_ETMX = power_rh_ETMX
        self.power_self_ETMX = power_self_ETMX

        # ETMY
        power_rh_ETMY = ezca['TCS-SIM_ETMY_SUB_DEFOCUS_RH_LF_OUTPUT']
        power_self_ETMY = ezca['TCS-SIM_ETMY_SUB_DEFOCUS_SELF_OUTPUT']

        dpdt_rh_ETMY = (power_rh_ETMY - self.power_rh_ETMY) / self.dt
        dpdt_self_ETMY = (power_self_ETMY - self.power_self_ETMY) / self.dt

        U_ETMY = array([[power_rh_ETMY], [dpdt_rh_ETMY], [power_self_ETMY], [dpdt_self_ETMY]])

        self.power_rh_ETMY = power_rh_ETMY
        self.power_self_ETMY = power_self_ETMY


        ## Kalman filtering
        X_prior_ITMX, P_prior_ITMX = kf_predictor(self.X_post_ITMX, self.P_post_ITMX,
        		U_ITMX, self.A_ITM, self.B_ITM, self.Q_ITM)
        self.X_post_ITMX, self.P_post_ITMX = kf_corrector(X_prior_ITMX, P_prior_ITMX,
        		Z_ITMX, self.C_ITM, self.R)

        X_prior_ITMY, P_prior_ITMY = kf_predictor(self.X_post_ITMY, self.P_post_ITMY,
        		U_ITMY, self.A_ITM, self.B_ITM, self.Q_ITM)
        self.X_post_ITMY, self.P_post_ITMY = kf_corrector(X_prior_ITMY, P_prior_ITMY,
        		Z_ITMY, self.C_ITM, self.R)

        X_prior_ETMX, P_prior_ETMX = kf_predictor(self.X_post_ETMX, self.P_post_ETMX,
        		U_ETMX, self.A_ETM, self.B_ETM, self.Q_ETM)
        self.X_post_ETMX, self.P_post_ETMX = kf_corrector(X_prior_ETMX, P_prior_ETMX,
        		Z_ETMX, self.C_ETM, self.R)

        X_prior_ETMY, P_prior_ETMY = kf_predictor(self.X_post_ETMY, self.P_post_ETMY,
        		U_ETMY, self.A_ETM, self.B_ETM, self.Q_ETM)
        self.X_post_ETMY, self.P_post_ETMY = kf_corrector(X_prior_ETMY, P_prior_ETMY,
        		Z_ETMY, self.C_ETM, self.R)

        ## Write KF results to channels
        Z_est_ITMX = dot(self.C_ITM, self.X_post_ITMX)
        Z_est_ITMY = dot(self.C_ITM, self.X_post_ITMY)
        Z_est_ETMX = dot(self.C_ETM, self.X_post_ETMX)
        Z_est_ETMY = dot(self.C_ETM, self.X_post_ETMY)

        ezca['TCS-SIM_ITMX_SUB_DEFOCUS_KALMAN_APOST_EST'] = Z_est_ITMX
        ezca['TCS-SIM_ITMY_SUB_DEFOCUS_KALMAN_APOST_EST'] = Z_est_ITMY
        ezca['TCS-SIM_ETMX_SUB_DEFOCUS_KALMAN_APOST_EST'] = Z_est_ETMX
        ezca['TCS-SIM_ETMY_SUB_DEFOCUS_KALMAN_APOST_EST'] = Z_est_ETMY
        #log('ITMX APOST estimate = %.6f' %(Z_est_ITMX) )

        ezca['TCS-SIM_ITMX_SUB_DEFOCUS_KALMAN_APOST_RES'] = Z_ITMX - Z_est_ITMX
        ezca['TCS-SIM_ITMY_SUB_DEFOCUS_KALMAN_APOST_RES'] = Z_ITMY - Z_est_ITMY
        ezca['TCS-SIM_ETMX_SUB_DEFOCUS_KALMAN_APOST_RES'] = Z_ETMX - Z_est_ETMX
        ezca['TCS-SIM_ETMY_SUB_DEFOCUS_KALMAN_APOST_RES'] = Z_ETMY - Z_est_ETMY
        #log('ITMX APOST residual = %.6f' %( 1000000* (Z_ITMX - Z_est_ITMX) ) )

        #log('Time remaining %.6f' %(self.dt -(time.time() - self.start) ) )
	if ( time.time() - self.start ) < self.dt :
        	time.sleep(self.dt - ( time.time() - self.start ) )
        #time.sleep(0.1)
        #log('Execution time is %.6f' %(time.time() - self.start) )
        self.start = time.time()
        return True

class INIT(GuardState):
    index = 0
    request = True
    def main(self):
        log('Initializing Kalman Filtering on TCS HWS data...')
        reload(tcsconfig)
        return True

    def run(self):
        return True


edges = [
    ('INIT', 'KAL_EST_RUNNING'),
    ('KAL_EST_RUNNING', 'STOP')
    ]

